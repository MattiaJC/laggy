import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import os
import subprocess

# stringhe dei comandi
# TODO: valutare se aggiungere SUDO all'inizio di ogni riga
comando: str = "tc qdisc change dev {r1} root netem delay {d1}ms loss {l1}%"
comando_inizio: str = "tc qdisc add dev {r1} root netem delay {d1}ms loss {l1}%"
comando_uscita: str = "tc qdisc delete dev {r1} root"


# builder gtk
builder: Gtk.Builder

# elementi grafica
main_window: Gtk.Window
dialog_root: Gtk.Dialog
dialog_info: Gtk.Dialog
dialog_rete: Gtk.Dialog
label_error: Gtk.Label

def esegui_comando( cmd:str):
    print("comando = ", cmd)
    out = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE).communicate()[1].__str__()
    label_error.set_text(out)

class Handler:
    # valori dei parametri
    rete: str = "lo"
    delay: float = 0
    loss: float = 0

    # elenco degli handler degli oggetti visualizzati
    def delay_cambiato(self, scale):
        self.delay = scale.get_value()
        #print("delay = ", self.delay, " loss = ", self.loss)
        #print("nuovo valore delay: ", delay, "ms")
        #print(comando.format(d1=delay, l1=loss))
        #os.system(comando.format(r1=rete,d1=delay, l1=loss))
        #out = os.popen(comando.format(r1=rete,d1=delay, l1=loss)).readline()
        esegui_comando(comando.format(r1=self.rete,d1=self.delay, l1=self.loss))
    
    def pck_loss_cambiato(self, scale):
        self.loss = scale.get_value()
        #print("delay = ", self.delay, " loss = ", self.loss)
        #print("nuovo valore pck loss: ", loss, "%")
        #print(comando.format(d1=delay, l1=loss))
        #os.system(comando.format(r1=rete,d1=delay, l1=loss))
        esegui_comando(comando.format(r1=self.rete,d1=self.delay, l1=self.loss))
    
    def finestra_mostrata(self, window):
        print("la finestra è mostrata")
        #print("inizio esecuzione, eseguo: ", comando_inizio.format(d1=delay, l1=loss))
        #os.system(comando_inizio.format(r1=rete,d1=delay, l1=loss))
        #out = subprocess.Popen(comando_inizio.format(r1=rete,d1=delay, l1=loss), shell=True, stderr=subprocess.PIPE).communicate()[1].__str__()
        esegui_comando(comando_inizio.format(r1=self.rete,d1=self.delay, l1=self.loss))
    
    def esci_programma(self, window):
        # triggero l'uscita dalla finestra, questo perché voglio che quando
        # esco venga eseguito uno script di sicurezza
        #print("devo uscire, eseguo: ", comando_uscita)
        #os.system(comando_uscita.format(r1=rete))
        out = os.popen(comando_uscita.format(r1=self.rete)).readline()
        Gtk.main_quit()
    
    # gestione degli input da menu
    def cambia_rete(self, menu_item, bottone):
        print("premuto <Cambia rete>")
        dialog_rete.show_all()
        main_window.set_sensitive(False)
    
    def mostra_info(self, menu_item, bottone):
        print("mostro info applicazione")
        print(dialog_info)
        # mostro il dialog 
        dialog_info.show_all()
        # disattivo la finestra principale
        main_window.set_sensitive(False)
    
    # gestione click dialog permessi root
    def esci_dialog_root(self, bottone):
        # funzione chiamata dal bottone all'interno del dialgo
        #print("devo uscire dal dialog")
        dialog_root.destroy()
        # questa chiusira richiama quindi la funzione sottostante
    
    def chiusura_dialog_root(self, dialog):
        # chiusura del dialog per i permessi di root da sistema
        # questa funzione viene chiamata quando si richiede la distruzione
        # del dialog
        #print("devo uscire dal dialog da sistema")
        # con questa funzione riabilitiamo la finestra principale
        main_window.set_sensitive(True)
    
    # gestione del dialog info app
    def esci_dialog_info(self, dialog):
        print("chiudo il dialog info app")
        # nel caso del dialog di info, se facessi destroy l'app crasha quando 
        # cerco di riaprirlo (non ho capito perché), per evitare ciò eseguo 
        # un hide
        dialog_info.hide()
        main_window.set_sensitive(True)
    
    def delete_event_info(self,dialog,event=None):
        # questo handler cattura il click del tasto 'X' per chiudere evitanto
        # che venga fatto la destroy del dialog
        print("premuto tasto chiudi")
        dialog_info.hide()
        main_window.set_sensitive(True)
        return True
    
    # gestione del dialog cambia rete
    def annulla_cambio_rete(self, button):
        # quando viene cliccato il tasto annulla del dialog
        dialog_rete.hide()
        main_window.set_sensitive(True)
    
    def conferma_cambio_rete(self, button):
        # quando viene cliccato il tasto OK del dialog
        # resetto la posizione degli slider in quanto questo implica ancora il trigger degli handler
        # e per questo non c'è neanche bisogno di resettare il valore dei parametri
        slider_delay.set_value(0)
        slider_pck_loss.set_value(0)
        # quando cambio la rete prima annullo le modifiche fatte alla rete in precedenza
        esegui_comando(comando_uscita.format(r1=self.rete))
        # cambio la rete
        self.rete = builder.get_object("entry_rete").get_text()
        print("nuova rete: ", self.rete)
        # eseguo una add di filtro su questa nuova rete
        esegui_comando(comando_inizio.format(r1=self.rete,d1=self.delay, l1=self.loss))
        # nascondo il dialog
        dialog_rete.hide()
        main_window.set_sensitive(True)
    
    def delete_event_rete(self,dialog,event=None):
        # come con il dialog info non vogliamo che vvenga eseguita la destroy
        dialog_rete.hide()
        main_window.set_sensitive(True)
        return True

builder = Gtk.Builder()
builder.add_from_file("./GUI/GUI.glade")
# imposto al builder qual'è la classe che contiene tutti gli handler 
# IMPOSTATI DA FILE 
builder.connect_signals(Handler())

# ottengo la main window 
main_window = builder.get_object("main_window")

# devo verificare che lo script sia eseguito da root, altrimenti i comandi
# non avranno effetto
if os.geteuid() != 0:
    print("Attenzione, non stai eseguendo da root")
    dialog_root = builder.get_object("dialog_root")
    dialog_root.show_all()
    main_window.set_sensitive(False)

# gestione slider per il delay
# ottengo lo slider (in GTK scale) dalla scena
slider_delay: Gtk.Scale = builder.get_object("slider_delay")
# imposto il range dello scale
slider_delay.set_range(0, 1000)

# gestione slider per il package loss
# ottengo lo slider
slider_pck_loss: Gtk.Scale = builder.get_object("slider_pck_loss")
# imposto il range dello scale
slider_pck_loss.set_range(0, 100)

# ottengo il dialog info dell'app
dialog_info = builder.get_object("dialog_info")

# ottengo il dialog per selezionare la rete
dialog_rete = builder.get_object("dialog_rete")

# ottengo la label degli errori
label_error = builder.get_object("errori_shell")

# connetto al "chiudi" della finestra che chiude il programma
#win.connect("destroy", Gtk.main_quit)

# mostro tutto 
main_window.show_all()
Gtk.main()
