# Laggy

## Cosa permette di fare questo software
Questo semplice script con interfaccia grafica permette di corrompere il funzionamento della rete dei sistemi operativi unix-like per poter simulare situazioni con LAG, perdita di pacchetti. Il LAG massimo impostabile è di 1s in quanto le chiamate di sistema impongono questi limiti. La perdita di pacchetti può essere impostata da 0% e 100%.

## Perché è stato realizzato
Questo script è stato realizzato durante la tesi magistrale per testare il corretto funzionamento del gioco realizzato in caso di rete non perfetta

## Avviare il software
Per avviare il software è necessario eseguirlo con i permessi da super utente
```
sudo python main.py
```
## What this software allows you to do
This simple script with a graphical interface allows you to corrupt the functioning of the network of unix-like operating systems in order to simulate situations with LAG, packet loss. The maximum LAG that can be set is 1s as system calls impose these limits. Packet loss can be set from 0% and 100%.

## Because it was made
This script was created during the master's thesis to test the correct functioning of the game created in case of a non-perfect network

## Start the software
To run the software you need to run it with super user permissions
```
sudo python main.py
```